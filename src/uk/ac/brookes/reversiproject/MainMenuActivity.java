package uk.ac.brookes.reversiproject;

import android.os.Bundle;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;

public class MainMenuActivity extends Activity {

	ContentResolver cr;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main_menu);
		
		final ImageButton mSinglePlayerButton = (ImageButton) findViewById(R.id.singleplayer_button);
		mSinglePlayerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent i = new Intent(getApplicationContext(), GameActivity.class);
            	i.putExtra("playerNumber", 1);
            	startActivity(i);
            }
        });
        
        final ImageButton mMutliPlayerButton = (ImageButton) findViewById(R.id.mutliplayer_button);
        mMutliPlayerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent i = new Intent(getApplicationContext(), GameActivity.class);
            	i.putExtra("playerNumber", 2);
            	startActivity(i);
            }
        });
		
        final ImageButton mHighScoresButton = (ImageButton) findViewById(R.id.highscore_button);
        mHighScoresButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent i = new Intent(getApplicationContext(), HighScoresActivity.class);
            	startActivity(i);
            }
        });
        
        final ImageButton mSettingsButton = (ImageButton) findViewById(R.id.settings_button);
        mSettingsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
            	startActivity(i);
            }
        });
        
        // Set Database for first run.
        cr = getContentResolver();
		
		Cursor c = cr.query( HighScoresProvider.CONTENT_URI, null, null, null, null );
		if( c.getCount() == 0 ) {
			populateProvider();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	private void populateProvider() {
		
		int[] ids = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
		
		int[] scores = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		
		ContentResolver cr = getContentResolver();

		ContentValues values = new ContentValues();
		for ( int i = 0; i < 10; i++ ) {
			values.put( HighScoresProvider.KEY_PLAYER_ID, ids[i] );
			values.put( HighScoresProvider.KEY_SCORE, scores[i] );
			cr.insert( HighScoresProvider.CONTENT_URI, values );
		}
	}

	
}
