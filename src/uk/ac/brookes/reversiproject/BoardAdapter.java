package uk.ac.brookes.reversiproject;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class BoardAdapter extends BaseAdapter {
	private Context mContext;
	int[] mBoard;
	
	int iPlayer1Tile;
	int iPlayer2Tile;
	
	public BoardAdapter( Context _context, int[] _board, int _tileSet ) {
		mContext = _context;
		this.mBoard = _board;
		
		switch( _tileSet ) {
		case 0:
			iPlayer1Tile = R.drawable.icon_a1_tile;
			iPlayer2Tile = R.drawable.icon_a2_tile;
			break;
		case 1:
			iPlayer1Tile = R.drawable.icon_b1_tile;
			iPlayer2Tile = R.drawable.icon_b2_tile;
			break;
		}
	}

	@Override
	public int getCount() {
		return mBoard.length;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
	    if ( convertView == null ) {
	          imageView = new ImageView( mContext );
	          imageView.setLayoutParams(new GridView.LayoutParams( ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT ));
	          imageView.setAdjustViewBounds( true );
	          imageView.setScaleType( ImageView.ScaleType.CENTER_CROP );
	          imageView.setPadding( 8, 8, 8, 8 );
	     } else {
	          imageView = (ImageView) convertView;
	     }
	    
	     switch ( mBoard[position] ) {
	          case 1: imageView.setImageResource( iPlayer1Tile ); break;
	          case 2: imageView.setImageResource( iPlayer2Tile ); break;
	          default: imageView.setImageResource( R.drawable.empty_tile ); break;

	      }
	      return imageView;
	}
	
	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}
}
