package uk.ac.brookes.reversiproject;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

public class HighScoresProvider extends ContentProvider {

	public static final String AUTHORITY = "uk.ac.brookes.reversiproject.highscores";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/highscores");

	private SQLiteDatabase highScoresDB;
	private static final String HIGHSCORES_TABLE = "highscores";
	public static final String KEY_ID = "_id";
	public static final String KEY_PLAYER_ID = "playerid";
	public static final String KEY_SCORE = "score";
	public static final int KEY_ID_COLUMN = 0;
	public static final int PLAYER_ID_COLUMN = 1;
	public static final int SCORE_COLUMN = 2;
	
	private static final int HIGHSCORES = 1;
	private static final int HIGHSCORE_ID = 2;
	private static final UriMatcher uriMatcher;
	
	private static final String DATABASE_NAME = "highscores.db";
	private static final int DATABASE_VERSION = 1;

	static {
		uriMatcher = new UriMatcher( UriMatcher.NO_MATCH );
		uriMatcher.addURI( AUTHORITY, "highscores", HIGHSCORES );
		uriMatcher.addURI( AUTHORITY, "highscores/#", HIGHSCORE_ID );
	}
	
	private String makeNewWhere(String _where, Uri _uri, int _matchResult ) {
		if ( _matchResult != HIGHSCORE_ID ) {
			return _where;
		} 
		else {
			String newWhereSoFar = KEY_ID + "=" + _uri.getPathSegments().get( 1 );
			if( TextUtils.isEmpty( _where ) )
				return newWhereSoFar;
			else
				return newWhereSoFar + " AND (" + _where + ')';
		}
	}

	
	@Override
	public int delete(Uri _uri, String _where, String[] _whereArgs) {
		int matchResult = uriMatcher.match( _uri );
		String newWhere = makeNewWhere( _where, _uri, matchResult);

		if ( matchResult == HIGHSCORES ) {
			int count = highScoresDB.delete( HIGHSCORES_TABLE, newWhere, _whereArgs );
			getContext().getContentResolver().notifyChange( _uri, null );
			return count;
		} 
		else
			throw new IllegalArgumentException( "Unknown URI " + _uri );

	}

	@Override
	public String getType( Uri _uri ) {
		switch ( uriMatcher.match( _uri ) ) {
		case HIGHSCORES:
			return "vnd.android.cursor.dir/vnd.brookes.name";
		default:
			throw new IllegalArgumentException( "Unsupported URI: " + _uri );
		}
	}

	@Override
	public Uri insert( Uri _uri, ContentValues _values ) {
		long rowID = highScoresDB.insert( HIGHSCORES_TABLE, KEY_PLAYER_ID, _values );
		if ( rowID > 0 ) {
			Uri newuri = ContentUris.withAppendedId( CONTENT_URI, rowID );
			getContext().getContentResolver().notifyChange( newuri, null );
			return _uri;
		} 
		else
			throw new SQLException( "Failed to insert row into " + _uri );

	}

	@Override
	public boolean onCreate() {
		HighScoresDatabaseHelper helper = new HighScoresDatabaseHelper( getContext(), DATABASE_NAME, null, DATABASE_VERSION );
		highScoresDB = helper.getWritableDatabase();
		return ( highScoresDB != null );
	}

	@Override
	public Cursor query( Uri _uri, String[] _projection, String _selection, String[] _selectionArgs, String _sort ) {
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		qb.setTables( HIGHSCORES_TABLE );

		if ( uriMatcher.match( _uri ) == HIGHSCORE_ID ) {
			qb.appendWhere( KEY_ID + "=" + _uri.getPathSegments().get( 1 ) );
		}

		Cursor c = qb.query( highScoresDB, _projection, _selection, _selectionArgs, null, null, _sort );

		c.setNotificationUri( getContext().getContentResolver(), _uri );

		return c;

	}

	@Override
	public int update(Uri _uri, ContentValues _values, String _where, String[] _whereArgs) {

		int matchResult = uriMatcher.match( _uri );
		String newWhere = makeNewWhere( _where, _uri, matchResult );

		if ( matchResult == HIGHSCORES ) {
			int count = highScoresDB.update(HIGHSCORES_TABLE, _values, newWhere, _whereArgs);
			
			getContext().getContentResolver().notifyChange( _uri, null );
			return count;
		} 
		else
			throw new IllegalArgumentException( "Unknown URI " + _uri );
	}

	private static class HighScoresDatabaseHelper extends SQLiteOpenHelper {

		public HighScoresDatabaseHelper(Context context, String name, CursorFactory factory, int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate( SQLiteDatabase _db ) {
			_db.execSQL("CREATE TABLE " + HIGHSCORES_TABLE + " (" + KEY_ID
					+ " INTEGER PRIMARY KEY AUTOINCREMENT, " +   
		                               KEY_PLAYER_ID + " INTEGER NOT NULL," + 
		                               KEY_SCORE + " INTEGER NOT NULL);");			
		}

		@Override
		public void onUpgrade( SQLiteDatabase _db, int _oldVersion, int _newVersion ) {
			_db.execSQL( "DROP TABLE IF EXISTS " + HIGHSCORES_TABLE );
			onCreate( _db );
		}

	}
}
