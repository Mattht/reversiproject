package uk.ac.brookes.reversiproject;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ContactAdapter extends ArrayAdapter<Contact> {

	Context mContext;
	
	LinearLayout.LayoutParams mPhotoParams;
	
	List<Contact> mContacts;
	
	public ContactAdapter(Context _context, int _textViewResourceId,
			List<Contact> _objects) {
		super(_context, _textViewResourceId, _objects);
		
		mPhotoParams = new LinearLayout.LayoutParams( 30, 30 );
		mPhotoParams.setMargins( 0, 0, 10, 0 );
		
		mContext = _context;
		mContacts = _objects;
	}
	
	public int getPosition( int _id ) {
		
		int iPosition = 0;
		
		for( int i = 0; i < mContacts.size(); i++ ){
			if( mContacts.get( i ).Id == _id ) {
				iPosition = i;
				break;
			}
		}
		
		return iPosition;
	}
	
	@Override
	public View getView( int _position, View _convertView, ViewGroup _parent ) {
		
		LinearLayout rowView = new LinearLayout( mContext );
		
		ImageView imageView = new ImageView( mContext );
		imageView.setLayoutParams( mPhotoParams );
		imageView.setImageBitmap( mContacts.get( _position ).Photo  );
		
		TextView displayText = new TextView( mContext );
		displayText.setHeight( 30 );
		displayText.setText( mContacts.get( _position ).Name );
		
		rowView.addView( imageView );
		rowView.addView( displayText );
		
		return rowView;
	}
	
	@Override
	public View getDropDownView( int _position, View _convertView, ViewGroup _parent ) {
		
		LinearLayout rowView = new LinearLayout( mContext );
		
		ImageView imageView = new ImageView( mContext );
		imageView.setLayoutParams( mPhotoParams );
		imageView.setImageBitmap( mContacts.get( _position ).Photo  );
		
		TextView displayText = new TextView( mContext );
		displayText.setHeight( 30 );
		displayText.setText( mContacts.get( _position ).Name );
		
		rowView.addView( imageView );
		rowView.addView( displayText );
		
		return rowView;
	}
}
