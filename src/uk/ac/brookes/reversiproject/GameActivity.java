package uk.ac.brookes.reversiproject;

import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


public class GameActivity extends Activity {
	
	// Settings
	boolean mbSoundEnabled;
	int miGameMode;
	int miTimer;
	int miTileSet;
	int miPlayer1;
	int miPlayer2;
	
	String msPlayer1Name;
	String msPlayer2Name; 
	
	// Setup
	private int[] mBoard = new int[64];
	
	int miTotalPlayers;
	int miCurrentPlayer;
	int miPlayer1Score;
	int miPlayer2Score;
	boolean mbCanSwap = false;
	
	GridView mgvBoardGame;
	BoardAdapter mBoardAdapter;
	TextView mtvCurrentPlayer;
	TextView mtvPlayer1Score;
	TextView mtvPlayer2Score;
	TextView mtvTimer;
	
	CountDownTimer mCountDownTimer;
	long mlPauseTimer;
	
	AlertDialog.Builder mEndGameDialogBuilder;
	
	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_game );
		
		// Set Options
		SharedPreferences mSettingsPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		
		mbSoundEnabled = mSettingsPrefs.getBoolean( "soundEnable", true );
		miGameMode = mSettingsPrefs.getInt( "gameMode", 0 );
		miTimer = mSettingsPrefs.getInt( "timer", 0 ) * 60 + 60;
		miTileSet = mSettingsPrefs.getInt( "tileset", 0 );
		miPlayer1 = mSettingsPrefs.getInt( "player1", 0 );
		miPlayer2 = mSettingsPrefs.getInt( "player2", 0 );

		Intent intent = getIntent();
		miTotalPlayers = intent.getIntExtra( "playerNumber", 1 );
		
		Cursor player1Cursor = getContentResolver().query( ContactsContract.Contacts.CONTENT_URI, 
				null, 
				ContactsContract.Contacts._ID +" = "+ miPlayer1,
				null, null );
		
		int nameIdx = player1Cursor.getColumnIndexOrThrow( ContactsContract.Contacts.DISPLAY_NAME );
		
		if( player1Cursor.moveToFirst() ) {
			msPlayer1Name = player1Cursor.getString( nameIdx );
		}
		else
			msPlayer1Name = "Player 1";
		player1Cursor.close();
		
		if( miTotalPlayers != 1 ) {
		
			Cursor player2Cursor = getContentResolver().query( ContactsContract.Contacts.CONTENT_URI, 
					null, 
					ContactsContract.Contacts._ID +" = "+ miPlayer2,
					null, null );
			
			nameIdx = player2Cursor.getColumnIndexOrThrow( ContactsContract.Contacts.DISPLAY_NAME );
			
			
			if( player2Cursor.moveToFirst() ) {
				msPlayer2Name = player2Cursor.getString( nameIdx );
			}
			else
				msPlayer2Name = "Player 2";
			player2Cursor.close();
		}
		else
			msPlayer2Name = "Wicket (AI)";
		
		ViewGroup layout = (ViewGroup) findViewById( R.id.main_layout );
		
		// Restart Game
		final AlertDialog.Builder restartDialogBuilder = new AlertDialog.Builder( this );
		restartDialogBuilder.setMessage( "Restart the game?" );
		restartDialogBuilder.setCancelable( false );
		restartDialogBuilder.setPositiveButton( "Yes" , new DialogInterface.OnClickListener() {
				public void onClick( DialogInterface dialog,int id ) {
					initGame();
				}
			  });
		restartDialogBuilder.setNegativeButton( "No", new DialogInterface.OnClickListener() {
				public void onClick( DialogInterface dialog, int id ) {
					startTimer( mlPauseTimer );
				}
		});
		
		final ImageButton restartButton = (ImageButton) findViewById( R.id.restart_button );
		restartButton.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
            	stopTimer();
            	AlertDialog alertDialog = restartDialogBuilder.create();
				alertDialog.show();	
            }
        });
		
		// Swap Player
		final AlertDialog.Builder swapDialogBuilder = new AlertDialog.Builder( this );
		swapDialogBuilder.setMessage( "Skip this turn?" );
		swapDialogBuilder.setCancelable( false );
		swapDialogBuilder.setPositiveButton( "Yes" , new DialogInterface.OnClickListener() {
				public void onClick( DialogInterface dialog,int id ) {
					changePlayer();
				}
			  });
		swapDialogBuilder.setNegativeButton( "No", new DialogInterface.OnClickListener() {
				public void onClick( DialogInterface dialog, int id ) {
					startTimer( mlPauseTimer );
				}
		});
		
		final ImageButton swapButton = (ImageButton) findViewById( R.id.swap_player_button );
		swapButton.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
            	
            	//if( mbCanSwap ) {
            		stopTimer();
	            	AlertDialog alertDialog = swapDialogBuilder.create();
					alertDialog.show();	
            	/*}
            	else {
            		CharSequence sText= "";
    				
    				if( miCurrentPlayer == 1 )
    					sText = msPlayer1Name + " still has legal moves";
    				else
    					sText = msPlayer2Name + " still has legal moves";

    				Toast toast = Toast.makeText( getApplicationContext(), sText, Toast.LENGTH_SHORT );
    				toast.show();
            	}*/
            }
        });
		
		// Quit
		final AlertDialog.Builder quitDialogBuilder = new AlertDialog.Builder( this );
		quitDialogBuilder.setMessage( "Quit the current game?" );
		quitDialogBuilder.setCancelable( false );
		quitDialogBuilder.setPositiveButton( "Yes" , new DialogInterface.OnClickListener() {
				public void onClick( DialogInterface dialog,int id ) {
					Intent intent = new Intent( getApplicationContext(), MainMenuActivity.class );
	            	startActivity( intent );
				}
			  });
		quitDialogBuilder.setNegativeButton( "No", new DialogInterface.OnClickListener() {
				public void onClick( DialogInterface dialog, int id ) {
					startTimer( mlPauseTimer );
				}
		});
		
		final ImageButton quitButton = (ImageButton) findViewById( R.id.pause_button );
		quitButton.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
            	stopTimer();
            	AlertDialog alertDialog = quitDialogBuilder.create();
				alertDialog.show();	
            }
        });
		
		// End Game
		mEndGameDialogBuilder = new AlertDialog.Builder( this );
		mEndGameDialogBuilder.setMessage( "Restart the game?" );
		
		mEndGameDialogBuilder.setCancelable( false );
		mEndGameDialogBuilder.setPositiveButton( "Yes" , new DialogInterface.OnClickListener() {
				public void onClick( DialogInterface dialog,int id ) {
					initGame();
				}
			  });
		mEndGameDialogBuilder.setNegativeButton( "No", new DialogInterface.OnClickListener() {
				public void onClick( DialogInterface dialog, int id ) {
					Intent intent = new Intent( getApplicationContext(), MainMenuActivity.class );
	            	startActivity( intent );
				}
		});
		
		mBoardAdapter = new BoardAdapter( this, mBoard, miTileSet );
		
		
		mgvBoardGame = new GridView( this ){
			
			@Override
			public void onMeasure( int widthMeasureSpec, int heightMeasureSpec )	// For square GridView
			{
				int iWantedSize = 0;
				
				if ( getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT )
					iWantedSize = widthMeasureSpec;
				else
					iWantedSize = heightMeasureSpec;
					
				super.onMeasure( iWantedSize, iWantedSize );
				
				this.setColumnWidth( (int) ( MeasureSpec.getSize( iWantedSize ) * 0.125 ) );
			}
		};
		
		mgvBoardGame.setId( 99999 );
		mgvBoardGame.setNumColumns( 8 );
		mgvBoardGame.setBackgroundResource( R.drawable.grid_background );
		mgvBoardGame.setStretchMode( GridView.NO_STRETCH );
		mgvBoardGame.setHorizontalSpacing( 0 );
		mgvBoardGame.setVerticalSpacing( 0 );
		mgvBoardGame.setClipChildren( true );
		mgvBoardGame.setPadding( 0, 0, 0, 0 );
		mgvBoardGame.setGravity( Gravity.CENTER_HORIZONTAL );
		
		
		mgvBoardGame.setAdapter( mBoardAdapter );
		
		setLayout( getResources().getConfiguration() );
		
		OnItemClickListener boardListener = new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				placeMove( position );
				
				checkMovesLeft();
				
			}
		};
		
		mgvBoardGame.setOnItemClickListener( boardListener );
		
		layout.addView( mgvBoardGame );
		
		mtvCurrentPlayer = (TextView) findViewById(R.id.current_player_textview);
		
		ImageView tileSetPlayer1 = (ImageView) findViewById( R.id.tileSet1ImageView );
		ImageView tileSetPlayer2 = (ImageView) findViewById( R.id.tileSet2ImageView );
		
		switch( miTileSet ) {	// 0 is by default
		case 1:
			tileSetPlayer1.setImageResource( R.drawable.icon_b1_tile );
			tileSetPlayer2.setImageResource( R.drawable.icon_b2_tile );
			break;
		}
		
		TextView tvPlayer1Name = (TextView) findViewById( R.id.player1_name );
		tvPlayer1Name.setText( msPlayer1Name );
		
		TextView tvPlayer2Name = (TextView) findViewById( R.id.player2_name );
		tvPlayer2Name.setText( msPlayer2Name );
		
		mtvPlayer1Score = (TextView) findViewById( R.id.player1_score );
		mtvPlayer2Score = (TextView) findViewById( R.id.player2_score );
		
		mtvTimer = (TextView) findViewById( R.id.timer_textview );
		
		if( miGameMode == 0 )
			mtvTimer.setText( "" );
		
		initGame();
	}
	
	void changePlayer() {
		if ( miCurrentPlayer == 1 ) {
			miCurrentPlayer = 2;
			mtvCurrentPlayer.setText( msPlayer2Name );
			
			if( miTotalPlayers == 1 ) {					// If in singleplayer relaunches AI
				checkMovesLeft();
			} 
			
		}
		else{
			miCurrentPlayer = 1;
			mtvCurrentPlayer.setText( msPlayer1Name );
		}
		
		stopTimer();
		mlPauseTimer = miTimer * 1000;
		startTimer( mlPauseTimer );
	}
	
	void initGame() {
		
		// Board
		for( int i = 0 ; i < mBoard.length ; i++ ) {
			mBoard[i] = 0;
		}
		mBoard[27] = 2;
		mBoard[28] = 1;
		mBoard[35] = 1;
		mBoard[36] = 2;
		
		mBoardAdapter.notifyDataSetChanged();
		
		miPlayer1Score = 2;
		miPlayer2Score = 2;
		mtvPlayer1Score.setText( String.valueOf( miPlayer1Score ) );
		mtvPlayer2Score.setText( String.valueOf( miPlayer2Score ) );
		
		miCurrentPlayer = 1;
		mtvCurrentPlayer.setText( msPlayer1Name );
		
		mlPauseTimer = miTimer * 1000;
		startTimer( mlPauseTimer );
	}
	
	void endGame() {
		
		int iWinningScore = 0;
		int iWinningPlayerId = 0;
		
		String sWinnerText = "";
		
		if( miPlayer1Score > miPlayer2Score ) {		// Player 1 wins
			iWinningScore = miPlayer1Score;
			iWinningPlayerId = miPlayer1;
			sWinnerText = msPlayer1Name + " wins! ";
		}
		else if( miPlayer1Score < miPlayer2Score ) {	// Player 2 wins
			iWinningScore = miPlayer2Score;
			iWinningPlayerId = miPlayer2;
			sWinnerText = msPlayer2Name + " wins! ";
		}
		else {
			sWinnerText = "It's a tie! ";
			iWinningScore = -1;
		}
			
		// AI may not be put in scoreboard 
		if( iWinningScore != -1 && ( miTotalPlayers == 2 || ( miTotalPlayers == 1 && miPlayer1Score >= miPlayer2Score ) ) ) {
		
			// Checks if players are from contact
			if( !msPlayer1Name.equals( "Player 1" ) && !msPlayer2Name.equals( "Player 2" ) ) {
			
				ContentResolver cr = getContentResolver();
				
				Cursor c = cr.query( HighScoresProvider.CONTENT_URI, null, null, null, HighScoresProvider.SCORE_COLUMN + " DESC" );
		
				if( c.moveToFirst() ) {
					do {
						if( iWinningScore > c.getInt( HighScoresProvider.SCORE_COLUMN ) ) {
							
							ContentValues values = new ContentValues();
							values.put( HighScoresProvider.KEY_PLAYER_ID, iWinningPlayerId );
							values.put( HighScoresProvider.KEY_SCORE, iWinningScore );
							cr.insert( HighScoresProvider.CONTENT_URI, values );
							
							if( c.moveToLast() ) {
								
								int keyId = c.getInt( HighScoresProvider.KEY_ID_COLUMN  );
								
								cr.delete( HighScoresProvider.CONTENT_URI, HighScoresProvider.KEY_ID +" = "+ keyId, null);
							}
							
							break;
						}
		            } while ( c.moveToNext() );
		        }
				c.close();
			
			}
		
		}
		
		mEndGameDialogBuilder.setMessage( sWinnerText + "\n\nRestart the game?" );

		AlertDialog alertDialog = mEndGameDialogBuilder.create();
		alertDialog.show();
		
	}
	
	// Start / Reset Timer
	void startTimer( long _lengthTimer ) {
		
		if( miGameMode == 1 ) {
			mCountDownTimer = new CountDownTimer( _lengthTimer, 1000 ) {
	
				public void onTick(long millisUntilFinished) {
					
					int seconds = (int) ( millisUntilFinished * 0.001 );
					
					mlPauseTimer = millisUntilFinished;
					mtvTimer.setText( String.valueOf( seconds ) );
					
					if( mbSoundEnabled && ( seconds ) % 30 == 0 ) {
						final ToneGenerator tg = new ToneGenerator( AudioManager.STREAM_NOTIFICATION, 100 );
					    tg.startTone( ToneGenerator.TONE_PROP_BEEP );
					}
				}
				
				public void onFinish() {
					mtvTimer.setText( "0" );
					
					endGame();
				}
			};
			
			mlPauseTimer = miTimer * 1000; // Reset for next time
			mCountDownTimer.start();
		}
	}
	
	void stopTimer() {
		if( miGameMode == 1 )
			mCountDownTimer.cancel();
	}
	
	void checkMovesLeft() {
		
		boolean bNextLegal = false;
		mbCanSwap = false;
		
		int keepPosition = 0;
		
		for( int i = 0 ; i < 64 ; i++ ) {				// Check if current player has any moves
			if( isLegalMove( i, miCurrentPlayer ) )
			{
				bNextLegal = true;
				keepPosition = i;
				
				break;
			}
		}
		
		mbCanSwap = !bNextLegal;
		
		if( !bNextLegal ) {								// Check For other player
			int iOtherPlayer;
			
			if( miCurrentPlayer == 1 )
				iOtherPlayer = 2;
			else
				iOtherPlayer = 1;
			
			for( int i = 0 ; i < 64 ; i++ ) {
				if( isLegalMove( i, iOtherPlayer ) )
				{
					bNextLegal = true;
					break;
				}
			}
			
			if( bNextLegal ) {							// Still no moves so game ends
				CharSequence sText= "";
				
				if( miCurrentPlayer == 1 )
					sText = "No more legal moves for "+ msPlayer1Name;
				else
					sText = "No more legal moves for "+ msPlayer2Name;

				Toast toast = Toast.makeText( getApplicationContext(), sText, Toast.LENGTH_SHORT );
				toast.show();
			}
			else {	// Game is ended !
				stopTimer();
				
				endGame();
			}
		}
		else {
			if( miTotalPlayers == 1 && miCurrentPlayer == 2 ) {
				placeMove( keepPosition );
			}
			
		}
		
	}
	
	boolean isLegalMove( int _position, int _player ) {	// Faster than placeMove. Returns ASAP
		
		if( mBoard[_position] == 0 )
		{
			int iIteration;
			
			int iLeftPosition = _position - 1;
			
			if( _position != 0 ) {
				iIteration = 0;
				while( (iLeftPosition % 8 + 8) % 8 != 7 /*&& leftPosition >= 0*/ && mBoard[iLeftPosition] != 0 && mBoard[iLeftPosition] != _player )
				{
					iLeftPosition--;
					iIteration++;
				}
				if( iIteration > 0 && iLeftPosition >= 0 && mBoard[iLeftPosition] == _player )
				{
					return true;
				}
			}
			
			int iRightPosition = _position + 1;
			
			if( _position != 63 ) {
				iIteration = 0;
				while( ( iRightPosition % 8 ) != 0 && mBoard[iRightPosition] != 0 && mBoard[iRightPosition] != _player )
				{
					iRightPosition++;
					iIteration++;
				}
				if( iIteration > 0 /* && rightPosition >= 0*/ && iRightPosition <= 63 && mBoard[iRightPosition] == _player )
				{
					return true;
				}
			}
			
			int iTopPosition = _position - 8;
			
			iIteration = 0;
			while( iTopPosition >= 0 && mBoard[iTopPosition] != 0 && mBoard[iTopPosition] != _player )
			{
				iTopPosition -= 8;
				iIteration++;
			}
			if( iIteration > 0 && iTopPosition >= 0 /* && topPosition <= 63*/ && mBoard[iTopPosition] == _player )
			{
				return true;
			}
			
			int iBottomPosition = _position + 8;
			
			iIteration = 0;
			while( iBottomPosition <= 63 && mBoard[iBottomPosition] != 0 && mBoard[iBottomPosition] != _player )
			{
				iBottomPosition += 8;
				iIteration++;
			}
			if( iIteration > 0 /*&& bottomPosition >= 0*/ && iBottomPosition <= 63 && mBoard[iBottomPosition] == _player )
			{
				return true;
			}
			
			// ---
			
			int iTopLeftPosition = _position - 9;
			
			iIteration = 0;
			while( iTopLeftPosition >= 0 && ( iTopLeftPosition % 8 ) != 7 && mBoard[iTopLeftPosition] != 0 && mBoard[iTopLeftPosition] != _player )
			{
				iTopLeftPosition -= 9;
				iIteration++;
			}
			if( iIteration > 0 && iTopLeftPosition >= 0 && iTopLeftPosition <= 63 && mBoard[iTopLeftPosition] == _player )
			{
				return true;
			}
			
			int iTopRightPosition = _position - 7;
			
			iIteration = 0;
			while( iTopRightPosition >= 0 && ( iTopRightPosition % 8 ) != 0 && mBoard[iTopRightPosition] != 0 && mBoard[iTopRightPosition] != _player )
			{
				iTopRightPosition -= 7;
				iIteration++;
			}
			if( iIteration > 0 && iTopRightPosition >= 0 && iTopRightPosition <= 63 && mBoard[iTopRightPosition] == _player )
			{
				return true;
			}
			
			int iBottomLeftPosition = _position + 7;
			
			iIteration = 0;
			while( iBottomLeftPosition <= 63 && ( iBottomLeftPosition % 8 ) != 7 && mBoard[iBottomLeftPosition] != 0 && mBoard[iBottomLeftPosition] != _player )
			{
				iBottomLeftPosition += 7;
				iIteration++;
			}
			if( iIteration > 0 && iBottomLeftPosition >= 0 && iBottomLeftPosition <= 63 && mBoard[iBottomLeftPosition] == _player )
			{
				return true;
			}
			
			int iBottomRightPosition = _position + 9;
			
			iIteration = 0;
			while( iBottomRightPosition <= 63 && ( iBottomRightPosition % 8 ) != 0 && mBoard[iBottomRightPosition] != 0 && mBoard[iBottomRightPosition] != _player )
			{
				iBottomRightPosition += 9;
				iIteration++;
			}
			if( iIteration > 0 && iBottomRightPosition >= 0 && iBottomRightPosition <= 63 && mBoard[iBottomRightPosition] == _player )
			{
				return true;
			}
		}
		
		return false;
	}
	
	
	void placeMove( int _position ) {	// Checks if move is valid
		
		boolean bIsLegal = false;
		
		int iIncreaseScore = 0;
		
		if( mBoard[_position] == 0 )
		{
			int iIteration;
			
			int iLeftPosition = _position - 1;
			
			if( _position != 0 ) {
				iIteration = 0;
				while( ( iLeftPosition % 8 ) != 7 && iLeftPosition >= 0 && mBoard[iLeftPosition] != 0 && mBoard[iLeftPosition] != miCurrentPlayer )
				{
					iLeftPosition--;
					iIteration++;
				}
				if( iIteration > 0 && iLeftPosition >= 0 /*&& leftPosition <= 63*/ && mBoard[iLeftPosition] == miCurrentPlayer )
				{
					for( int j = iLeftPosition; j != _position; j++ )
					{
						if( mBoard[j] != miCurrentPlayer ) {
							mBoard[j] = miCurrentPlayer;
							iIncreaseScore++;
						}
					}
					bIsLegal = true;
				}
			}
			
			int iRightPosition = _position + 1;
			
			if( _position != 63 ) {
				iIteration = 0;
				while( ( iRightPosition % 8 ) != 0 && mBoard[iRightPosition] != 0 && mBoard[iRightPosition] != miCurrentPlayer )
				{
					iRightPosition++;
					iIteration++;
				}
				if( iIteration > 0 /* && rightPosition >= 0*/ && iRightPosition <= 63 && mBoard[iRightPosition] == miCurrentPlayer )
				{
					for( int j = iRightPosition; j != _position; j-- )
					{
						if( mBoard[j] != miCurrentPlayer ) {
							mBoard[j] = miCurrentPlayer;
							iIncreaseScore++;
						}
					}
					bIsLegal = true;
				}
			}
			
			int iTopPosition = _position - 8;
			
			iIteration = 0;
			while( iTopPosition >= 0 && mBoard[iTopPosition] != 0 && mBoard[iTopPosition] != miCurrentPlayer )
			{
				iTopPosition -= 8;
				iIteration++;
			}
			if( iIteration > 0 && iTopPosition >= 0 /* && topPosition <= 63*/ && mBoard[iTopPosition] == miCurrentPlayer )
			{
				for( int j = iTopPosition; j != _position; j = j + 8 )
				{
					if( mBoard[j] != miCurrentPlayer ) {
						mBoard[j] = miCurrentPlayer;
						iIncreaseScore++;
					}
				}
				bIsLegal = true;
			}
			
			int iBottomPosition = _position + 8;
			
			iIteration = 0;
			while( iBottomPosition <= 63 && mBoard[iBottomPosition] != 0 && mBoard[iBottomPosition] != miCurrentPlayer )
			{
				iBottomPosition += 8;
				iIteration++;
			}
			if( iIteration > 0 /*&& bottomPosition >= 0*/ && iBottomPosition <= 63 && mBoard[iBottomPosition] == miCurrentPlayer )
			{
				for( int j = iBottomPosition; j != _position; j = j - 8 )
				{
					if( mBoard[j] != miCurrentPlayer ) {
						mBoard[j] = miCurrentPlayer;
						iIncreaseScore++;
					}
				}
				bIsLegal = true;
			}
			
			// ---
			
			int iTopLeftPosition = _position - 9;
			
			iIteration = 0;
			while( iTopLeftPosition >= 0 && ( iTopLeftPosition % 8 ) != 7 && mBoard[iTopLeftPosition] != 0 && mBoard[iTopLeftPosition] != miCurrentPlayer )
			{
				iTopLeftPosition -= 9;
				iIteration++;
			}
			if( iIteration > 0 && iTopLeftPosition >= 0 && iTopLeftPosition <= 63 && mBoard[iTopLeftPosition] == miCurrentPlayer )
			{
				for( int j = iTopLeftPosition; j != _position; j = j + 9 )
				{
					if( mBoard[j] != miCurrentPlayer ) {
						mBoard[j] = miCurrentPlayer;
						iIncreaseScore++;
					}
				}
				bIsLegal = true;
			}
			
			int iTopRightPosition = _position - 7;
			
			iIteration = 0;
			while( iTopRightPosition >= 0 && ( iTopRightPosition % 8 ) != 0 && mBoard[iTopRightPosition] != 0 && mBoard[iTopRightPosition] != miCurrentPlayer )
			{
				iTopRightPosition -= 7;
				iIteration++;
			}
			if( iIteration > 0 && iTopRightPosition >= 0 && iTopRightPosition <= 63 && mBoard[iTopRightPosition] == miCurrentPlayer )
			{
				for( int j = iTopRightPosition; j != _position; j = j + 7 )
				{
					if( mBoard[j] != miCurrentPlayer ) {
						mBoard[j] = miCurrentPlayer;
						iIncreaseScore++;
					}
				}
				bIsLegal = true;
			}
			
			int iBottomLeftPosition = _position + 7;
			
			iIteration = 0;
			while( iBottomLeftPosition <= 63 && ( iBottomLeftPosition % 8 ) != 7 && mBoard[iBottomLeftPosition] != 0 && mBoard[iBottomLeftPosition] != miCurrentPlayer )
			{
				iBottomLeftPosition += 7;
				iIteration++;
			}
			if( iIteration > 0 && iBottomLeftPosition >= 0 && iBottomLeftPosition <= 63 && mBoard[iBottomLeftPosition] == miCurrentPlayer )
			{
				for( int j = iBottomLeftPosition; j != _position; j = j - 7 )
				{
					if( mBoard[j] != miCurrentPlayer ) {
						mBoard[j] = miCurrentPlayer;
						iIncreaseScore++;
					}
				}
				bIsLegal = true;
			}
			
			int iBottomRightPosition = _position + 9;
			
			iIteration = 0;
			while( iBottomRightPosition <= 63 && ( iBottomRightPosition % 8 ) != 0 && mBoard[iBottomRightPosition] != 0 && mBoard[iBottomRightPosition] != miCurrentPlayer )
			{
				iBottomRightPosition += 9;
				iIteration++;
			}
			if( iIteration > 0 && iBottomRightPosition >= 0 && iBottomRightPosition <= 63 && mBoard[iBottomRightPosition] == miCurrentPlayer )
			{
				for( int j = iBottomRightPosition; j != _position; j = j - 9 )
				{
					if( mBoard[j] != miCurrentPlayer ) {
						mBoard[j] = miCurrentPlayer;
						iIncreaseScore++;
					}
				}
				bIsLegal = true;
			}
			
			iIncreaseScore++;
			
		}
		
		if( bIsLegal )
		{
			
			if ( miCurrentPlayer == 1 ) {
				miPlayer1Score += iIncreaseScore;
				mtvPlayer1Score.setText( String.valueOf( miPlayer1Score ) );
				
				miPlayer2Score -= ( iIncreaseScore - 1 );
				mtvPlayer2Score.setText( String.valueOf( miPlayer2Score ) );
				
			}
			else{
				miPlayer1Score -=  ( iIncreaseScore - 1 );
				mtvPlayer1Score.setText( String.valueOf( miPlayer1Score ) );
				
				miPlayer2Score += iIncreaseScore;
				mtvPlayer2Score.setText( String.valueOf( miPlayer2Score ) );
				
			}
			
			mBoard[_position] = miCurrentPlayer;
			
			mBoardAdapter.notifyDataSetChanged();
			
			changePlayer();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game, menu);
		return true;
	}
	
	void setLayout( Configuration _config ){		// If there is a need of support landscape
		
		if( _config.orientation == Configuration.ORIENTATION_PORTRAIT ) {
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams( ViewGroup.LayoutParams.MATCH_PARENT,
			        ViewGroup.LayoutParams.WRAP_CONTENT);
			
			params.setMargins( 0, 10, 0, 0);

			params.addRule(RelativeLayout.BELOW, R.id.pause_button);
			mgvBoardGame.setLayoutParams(params);
		}
		else if( _config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams( ViewGroup.LayoutParams.WRAP_CONTENT,
			        ViewGroup.LayoutParams.WRAP_CONTENT);
			mgvBoardGame.setLayoutParams(params);
		}
		
		
	}
	
	/* Removed: No need for this
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);
	    
	    setLayout( newConfig );
	}
	*/
}
