package uk.ac.brookes.reversiproject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.ToggleButton;

class Contact {
	
	public int Id;
	public String Name;
	public Bitmap Photo;
	
	public Contact( int _id, String _name ){
		Id = _id;
		Name = _name;
	}
}

class ContactFiller extends AsyncTask<Void, Void, Long> {

	Context context;
	
	List<Contact> aContactArray;
	List<Contact> tmpContactArray;
	
	ContactAdapter adapter;
	
	Spinner mPlayer1Spinner;
	Spinner mPlayer2Spinner;
	
	int iPlayer1;
	int iPlayer2;
	
	public ContactFiller( Context _context, List<Contact> _contactArray, ContactAdapter _adapter, Spinner _player1Spinner, Spinner _player2Spinner, int _player1, int _player2 ) {
		
		context = _context;
		
		aContactArray = _contactArray;
		tmpContactArray = new ArrayList<Contact>();
		
		mPlayer1Spinner = _player1Spinner;
		mPlayer2Spinner = _player2Spinner;
		
		iPlayer1 = _player1;
		iPlayer2 = _player2;
		
		adapter = _adapter;
	}
	
	@Override
	protected Long doInBackground(Void... arg0) {
		
		Cursor cursor = context.getContentResolver().query( ContactsContract.Contacts.CONTENT_URI, null, null, null, null );

	    int idIdx = cursor.getColumnIndexOrThrow( ContactsContract.Contacts._ID );
	    int nameIdx= cursor.getColumnIndexOrThrow( ContactsContract.Contacts.DISPLAY_NAME );
	    
	    
	    if( cursor.moveToFirst() ) {
	    	do {
	    		Contact newContact = new Contact( cursor.getInt( idIdx ), cursor.getString( nameIdx ) );
	    		
	    		Uri person = ContentUris.withAppendedId(
	   				 ContactsContract.Contacts.CONTENT_URI,
					 Long.parseLong( cursor.getString( idIdx ) ) );
					
	    		InputStream photoStream = 
	                     ContactsContract.Contacts.openContactPhotoInputStream(
	                    		 context.getContentResolver(), person);
					
	    		if (photoStream != null) {
            		Bitmap photo = BitmapFactory.decodeStream(photoStream);
            		newContact.Photo = photo;
            		
	            	try {
                    	photoStream.close();
	            	} catch ( IOException e ) { 
	            		e.printStackTrace();
	            	}
    			} else {
    				newContact.Photo = BitmapFactory.decodeResource( context.getResources(), R.drawable.ic_launcher);
    			}
	    		
	    		tmpContactArray.add( newContact );
	    		
	    		
	    	} while ( cursor.moveToNext() );
	    	
	    	cursor.close();
	    }
	    
		return null;
	}
	
	protected void onPostExecute(Long result) {
		
		aContactArray.clear();
		
		for(Contact s : tmpContactArray)
			aContactArray.add(s);
		
		adapter.notifyDataSetChanged();
		
		mPlayer1Spinner.setSelection( adapter.getPosition( iPlayer1 ) );
		mPlayer2Spinner.setSelection( adapter.getPosition( iPlayer2 ) );
    }
	
}

public class SettingsActivity extends Activity {

	boolean mbSoundEnabled;
	int miGameMode;
	int miTimer;
	int miTileSet;
	int miPlayer1;
	int miPlayer2;
	
	ContactAdapter adapter;
	
	List<Contact> aContactArray;
	
	ToggleButton mTileSet1ToggleButton;
	ToggleButton mTileSet2ToggleButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		
		SharedPreferences mSettingsPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		final SharedPreferences.Editor mSettingsEditor = mSettingsPrefs.edit();
		
		mbSoundEnabled = mSettingsPrefs.getBoolean( "soundEnable", true );
		miGameMode = mSettingsPrefs.getInt( "gameMode", 0 );
		miTimer = mSettingsPrefs.getInt( "timer", 0 );
		miTileSet = mSettingsPrefs.getInt( "tileset", 0 );
		miPlayer1 = mSettingsPrefs.getInt( "player1", 0 );
		miPlayer2 = mSettingsPrefs.getInt( "player2", 0 );
		
		//-- Template
		
		ToggleButton mSoundToggleButton = (ToggleButton) findViewById( R.id.soundToggleButton );
		RadioGroup mModeRadioGroup = (RadioGroup) findViewById( R.id.modeRadioGroup );
		Spinner	mTimerSpinner = (Spinner) findViewById( R.id.timerSpinner );
		mTileSet1ToggleButton = (ToggleButton) findViewById( R.id.tilset1Toggle );
		mTileSet2ToggleButton = (ToggleButton) findViewById( R.id.tilset2Toggle );
	    Spinner mPlayer1Spinner = (Spinner) findViewById( R.id.player1Spinner );
	    Spinner mPlayer2Spinner = (Spinner) findViewById( R.id.player2Spinner );
	    
	    //-- Setup
	    
	    // Save
	    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder( this );
			alertDialogBuilder.setMessage( "Save settings before?" );
			alertDialogBuilder.setCancelable( false );
			alertDialogBuilder.setPositiveButton( "Yes" , new DialogInterface.OnClickListener() {
					public void onClick( DialogInterface dialog,int id ) {
						
						mSettingsEditor.putBoolean( "soundEnable", mbSoundEnabled );
						mSettingsEditor.putInt( "gameMode", miGameMode  );
						mSettingsEditor.putInt( "timer", miTimer );
						mSettingsEditor.putInt( "tileset", miTileSet );
						mSettingsEditor.putInt( "player1", miPlayer1 );
						mSettingsEditor.putInt( "player2", miPlayer2 );
						mSettingsEditor.commit();
						
						Intent i = new Intent( getApplicationContext(), MainMenuActivity.class );
		            	startActivity(i);
					}
				  });
			alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
					public void onClick( DialogInterface dialog, int id ) {
						Intent i = new Intent(getApplicationContext(), MainMenuActivity.class);
		            	startActivity(i);
					}
				});
	    
	    final ImageButton mSaveButton = (ImageButton) findViewById( R.id.saveButton );
	    mSaveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();	
            }
        });
	    
	    // Sound
	    mSoundToggleButton.setChecked( mbSoundEnabled );
	    mSoundToggleButton.setOnCheckedChangeListener( new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				mbSoundEnabled = isChecked;
				
			}
		});
	    
	    // Game Mode
	    if( miGameMode == 0 )
	    	mModeRadioGroup.check( R.id.normalModeRadio );
	    else
	    	mModeRadioGroup.check( R.id.timedModeRadio );
	    
	    mModeRadioGroup.setOnCheckedChangeListener( new RadioGroup.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				
				if( checkedId == R.id.normalModeRadio )
					miGameMode = 0;
				else
					miGameMode = 1;
			}
		} );
	    
	    // Timer
	    mTimerSpinner.setSelection( miTimer );
	    mTimerSpinner.setOnItemSelectedListener( new OnItemSelectedListener() {

			@Override
			public void onItemSelected( AdapterView<?> arg0, View arg1, int arg2, long arg3 ) 
			{	
				miTimer = arg2;
			}

			@Override
			public void onNothingSelected( AdapterView<?> arg0 ) { }
	    	
		});
	    
	    // Tileset
	    switch( miTileSet ) {
	    case 0:
	    	mTileSet1ToggleButton.setChecked( true );
	    	break;
	    case 1:
	    	mTileSet2ToggleButton.setChecked( true );
	    	break;
	    }
	    
	    mTileSet1ToggleButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

	    	@Override
	    	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	    		if( isChecked ) {
	    			mTileSet2ToggleButton.setChecked( false );
	    			miTileSet = 0;
	    		}
	    		else {
	    			mTileSet2ToggleButton.setChecked( true );
	    			miTileSet = 1;
	    		}
	    	}
        });
	    
	    mTileSet2ToggleButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

	    	@Override
	    	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	    		if( isChecked ) {
	    			mTileSet1ToggleButton.setChecked( false );
	    			miTileSet = 1;
	    		}
	    		else {
	    			mTileSet1ToggleButton.setChecked( true );
    				miTileSet = 0;
	    		}
	    	}
        });
	    
	    // Players
	    aContactArray =  new ArrayList<Contact>();
	    
	    Contact tmpContact = new Contact( 1, "Loading..." );
	    tmpContact.Photo = BitmapFactory.decodeResource( getResources(), R.drawable.ic_launcher );
	    aContactArray.add( tmpContact );
	    
	    adapter = new ContactAdapter( this, android.R.layout.simple_spinner_item, aContactArray );
	    
	    ContactFiller contactFiller = new ContactFiller( this, aContactArray, adapter, mPlayer1Spinner, mPlayer2Spinner, miPlayer1, miPlayer2 );
	    contactFiller.execute();
	    
	    // Player 1
	    mPlayer1Spinner.setAdapter( adapter );
	    
	    mPlayer1Spinner.setOnItemSelectedListener( new OnItemSelectedListener() {

			@Override
			public void onItemSelected( AdapterView<?> arg0, View arg1, int arg2, long arg3 ) {
				miPlayer1 = adapter.mContacts.get( arg2 ).Id;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) { }
	    	
		});
	    
	    
	    // Player 2
	    mPlayer2Spinner.setAdapter( adapter );
	    
	    mPlayer2Spinner.setOnItemSelectedListener( new OnItemSelectedListener() {

			@Override
			public void onItemSelected( AdapterView<?> arg0, View arg1, int arg2, long arg3 ) {
				miPlayer2 = adapter.mContacts.get( arg2 ).Id;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) { }
	    	
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}

}
