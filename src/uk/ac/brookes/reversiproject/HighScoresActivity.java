package uk.ac.brookes.reversiproject;

import java.io.IOException;
import java.io.InputStream;

import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class HighScoresActivity extends Activity {

	int miNumberScores;
	ContentResolver cr;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_high_scores);
		
		final ImageButton mBackButton = (ImageButton) findViewById( R.id.backButton );
		mBackButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent( getApplicationContext(), MainMenuActivity.class );
            	startActivity( intent );
            }
        });
		
		cr = getContentResolver();
		
		String[] defaultNames = { "Luke", "Obi-Wan", "Han", "Revan",
				"Lando", "Leia", "Chewi", "Yoda", 
                                         "Ahsoka", "Wicket" };
		
		Cursor c = cr.query( HighScoresProvider.CONTENT_URI, null, null, null, HighScoresProvider.KEY_SCORE + " DESC" );
		
		if( c.moveToFirst() ) {
			do {
				
				TextView tvName = (TextView) findViewById( R.id.nameTextView1 );
				TextView tvScore = (TextView) findViewById( R.id.scoreTextView1 );
				ImageView ivPhoto = (ImageView) findViewById( R.id.scoreImageView1 );
				
				String sName = "";
				String sScore = "";
				Bitmap photo = BitmapFactory.decodeResource( getResources(), R.drawable.ic_launcher);; 
				
				if( c.getInt( HighScoresProvider.PLAYER_ID_COLUMN ) == -1 ) {
					
					sName = defaultNames[ c.getPosition() ];
					sScore = c.getString( HighScoresProvider.SCORE_COLUMN );
					
				}
				else
				{
					
					int playerId = c.getInt( HighScoresProvider.PLAYER_ID_COLUMN );
					
					Cursor cursor = getContentResolver().query( ContactsContract.Contacts.CONTENT_URI, 
							null, 
							ContactsContract.Contacts._ID +" = "+ playerId, 
							null, null );
					
					int nameIdx = cursor.getColumnIndexOrThrow( ContactsContract.Contacts.DISPLAY_NAME );
					
					cursor.moveToPosition( 0 );
					
					sName = cursor.getString( nameIdx );
					sScore = c.getString( HighScoresProvider.SCORE_COLUMN );
					
					cursor.close();
					
					Uri person = ContentUris.withAppendedId(
			   				 ContactsContract.Contacts.CONTENT_URI,
			   				 playerId );
							
		    		InputStream photoStream = 
		                     ContactsContract.Contacts.openContactPhotoInputStream(
		                    		 cr, person);
						
		    		if (photoStream != null) {
		    			photo = BitmapFactory.decodeStream(photoStream);
		    			
		            	try {
	                    	photoStream.close();
		            	} catch ( IOException e ) { 
		            		e.printStackTrace();
		            	}
	    			}
				}
				
				switch( c.getPosition() ) {
				case 0:
					break;
				case 1:
					tvName = (TextView) findViewById( R.id.nameTextView2 );
					tvScore = (TextView) findViewById( R.id.scoreTextView2 );
					ivPhoto = (ImageView) findViewById( R.id.scoreImageView2 );
					break;
				case 2:
					tvName = (TextView) findViewById( R.id.nameTextView3 );
					tvScore = (TextView) findViewById( R.id.scoreTextView3 );
					ivPhoto = (ImageView) findViewById( R.id.scoreImageView3 );
					break;
				case 3:
					tvName = (TextView) findViewById( R.id.nameTextView4 );
					tvScore = (TextView) findViewById( R.id.scoreTextView4 );
					ivPhoto = (ImageView) findViewById( R.id.scoreImageView4 );
					break;
				case 4:
					tvName = (TextView) findViewById( R.id.nameTextView5 );
					tvScore = (TextView) findViewById( R.id.scoreTextView5 );
					ivPhoto = (ImageView) findViewById( R.id.scoreImageView5 );
					break;
				case 5:
					tvName = (TextView) findViewById( R.id.nameTextView6 );
					tvScore = (TextView) findViewById( R.id.scoreTextView6 );
					ivPhoto = (ImageView) findViewById( R.id.scoreImageView6 );
					break;
				case 6:
					tvName = (TextView) findViewById( R.id.nameTextView7 );
					tvScore = (TextView) findViewById( R.id.scoreTextView7 );
					ivPhoto = (ImageView) findViewById( R.id.scoreImageView7 );
					break;
				case 7:
					tvName = (TextView) findViewById( R.id.nameTextView8 );
					tvScore = (TextView) findViewById( R.id.scoreTextView8 );
					ivPhoto = (ImageView) findViewById( R.id.scoreImageView8 );
					break;
				case 8:
					tvName = (TextView) findViewById( R.id.nameTextView9 );
					tvScore = (TextView) findViewById( R.id.scoreTextView9 );
					ivPhoto = (ImageView) findViewById( R.id.scoreImageView9 );
					break;
				default:
					tvName = (TextView) findViewById( R.id.nameTextView10 );
					tvScore = (TextView) findViewById( R.id.scoreTextView10 );
					ivPhoto = (ImageView) findViewById( R.id.scoreImageView10 );
					break;
				}
				
				tvName.setText( sName );
				tvScore.setText( sScore );
				ivPhoto.setImageBitmap( photo );
				
            } while (c.moveToNext());
        }
		c.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.high_scores, menu);
		return true;
	}

}
